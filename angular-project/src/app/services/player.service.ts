import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders} from '@angular/common/http';
//import { Response } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Player } from '../models/player.inteface';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';

//const PLAYER_API: string = 'assets/json/player.json';
//const PLAYER_API: string = 'api/players';

@Injectable()
export class PlayerService {

    constructor(private http: HttpClient) {
        
    }

    getPlayers(): Observable<Player[]> {
        return this.http
            //.get<Player[]>(environment.PLAYER_API);
            .get<Player[]>(`${environment.PLAYER_API}/all`);
            //.get<Player[]>("http://localhost:8080/players/all");
    }

    removePlayer(id: Number): Observable<Player> {
        return this.http
            .delete<Player>(`${environment.PLAYER_API}/${id}`);
    }

    getPlayer(id: Number): Observable<Player> {
        return this.http
            .get<Player>(`${environment.PLAYER_API}/${id}`);
    }

    updatePlayer(id: number, value: Player): Observable<Player> {
        return this.http.put<Player>(`${environment.PLAYER_API}/${id}`, value);
    }

    createPlayer(player: Player): Observable<Player> {
        return this.http.post<Player>(`${environment.PLAYER_API}/`, player);
    }
}