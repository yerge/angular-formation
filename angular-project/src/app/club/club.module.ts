import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClubComponent } from './club.component';
import { ClubRoutingModule } from './club-routing.module';

@NgModule({
  declarations: [ClubComponent],
  imports: [
    CommonModule,
    ClubRoutingModule
  ],
  exports: [
    
  ]
})
export class ClubModule { }
