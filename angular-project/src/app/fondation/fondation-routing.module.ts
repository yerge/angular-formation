import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FondationComponent } from './fondation.component';

const routes: Routes = [
    {
      path: '',
      component: FondationComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class FondationRoutingModule { }