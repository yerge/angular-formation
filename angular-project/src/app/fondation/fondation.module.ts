import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FondationRoutingModule } from './fondation-routing.module';
import { FondationComponent } from './fondation.component';

@NgModule({
  declarations: [FondationComponent],
  imports: [
    CommonModule,
    FondationRoutingModule
  ]
})
export class FondationModule { }
