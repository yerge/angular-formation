import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService {

  constructor() { }

  createDb() {
    const players = [
      {
        "id": 0,
        "name": "Kevin Philipps",
        "age": 24,
        "position": "Midfielder",
        "height": 1.78,
        "weight": 72,
        "club": {
            "name": "Leeds United",
            "creationYear": 1919
          },
        "numberOfGoals": 1,
        "numberOfYellowCards": 9,
        "numberOfRedCards": 1
      },
      {
        "id": 1,
        "name": "Pablo Hernández Domínguez",
        "age": 35,
        "position": "Midfielder",
        "height": 1.73,
        "weight": 64,
        "club": {
            "name": "Leeds United",
            "creationYear": 1919
          },
        "numberOfGoals": 1,
        "numberOfYellowCards": 9,
        "numberOfRedCards": 1
      },
      {
        "id": 2,
        "name": "Kiko Casilla",
        "age": 33,
        "position": "Goalkeeper ",
        "height": 1.91,
        "weight": 90,
        "club": {
            "name": "Leeds United",
            "creationYear": 1919
          },
        "numberOfGoals": 0,
        "numberOfYellowCards": 2,
        "numberOfRedCards": 1
      },
      {
        "id": 3,
        "name": "Patrick Bamford",
        "age": 26,
        "position": "Forward",
        "height": 1.85,
        "weight": 71,
        "club": {
            "name": "Leeds United",
            "creationYear": 1919
          },
        "numberOfGoals": 9,
        "numberOfYellowCards": 3,
        "numberOfRedCards": 0
      },
      {
        "id": 4,
        "name": "Ben White",
        "age": 22,
        "position": "Defender",
        "height": 1.85,
        "weight": 78,
        "club": {
            "name": "Leeds United",
            "creationYear": 1919
          },
        "numberOfGoals": 1,
        "numberOfYellowCards": 1,
        "numberOfRedCards": 0
      },
      {
        "id": 5,
        "name": "Jack Harrison",
        "age": 23,
        "position": "Midfielder",
        "height": 1.75,
        "weight": 70,
        "club": {
            "name": "Leeds United",
            "creationYear": 1919
          },
        "numberOfGoals": 4,
        "numberOfYellowCards": 2,
        "numberOfRedCards": 0
      },
      {
        "id": 6,
        "name": "Ian Poveda",
        "age": 20,
        "position": "Forward",
        "height": 1.65,
        "weight": 60,
        "club": {
            "name": "Leeds United",
            "creationYear": 1919
          },
        "numberOfGoals": 1,
        "numberOfYellowCards": 1,
        "numberOfRedCards": 0
      },
      {
        "id": 7,
        "name": "Liam Cooper",
        "age": 28,
        "position": "Defender",
        "height": 1.88,
        "weight": 73,
        "club": {
            "name": "Leeds United",
            "creationYear": 1919
          },
        "numberOfGoals": 0,
        "numberOfYellowCards": 1,
        "numberOfRedCards": 0
      },
      {
        "id": 8,
        "name": "Pascal Struijk",
        "age": 20,
        "position": "Defender",
        "height": 1.9,
        "weight": 73,
        "club": {
            "name": "Leeds United",
            "creationYear": 1919
          },
        "numberOfGoals": 0,
        "numberOfYellowCards": 0,
        "numberOfRedCards": 0
      },
      {
        "id": 9,
        "name": "Gaetano Berardi",
        "age": 31,
        "position": "Defender",
        "height": 1.78,
        "weight": 72,
        "club": {
            "name": "Leeds United",
            "creationYear": 1919
          },
        "numberOfGoals": 0,
        "numberOfYellowCards": 6,
        "numberOfRedCards": 1
      },
      {
        "id": 10,
        "name": "Mateusz Klich",
        "age": 30,
        "position": "Midfielder",
        "height": 1.83,
        "weight": 84,
        "club": {
            "name": "Leeds United",
            "creationYear": 1919
          },
        "numberOfGoals": 0,
        "numberOfYellowCards": 2,
        "numberOfRedCards": 0
      }
    ];
    return {players};
  }
}
