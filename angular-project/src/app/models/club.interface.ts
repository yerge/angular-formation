export interface Club {
    name: string;
    creationYear: number;
}