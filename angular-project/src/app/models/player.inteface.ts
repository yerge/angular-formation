import { Club } from './club.interface';

export class Player {
    id: number;
    name: string;
    age:  number;
    position: string;
    height: number | null;
    weight: number | null;
    club: Club | null;
    numberOfGoals: number | null;
    numberOfYellowCards: number | null;
    numberOfRedCards: number | null;
}