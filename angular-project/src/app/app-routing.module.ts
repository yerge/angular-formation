import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

const appRoutes = [
    {
        path: 'home',
        loadChildren: () => import('./home/home.module').then(m => m.HomeModule)
    },
    {
        path: 'club',
        loadChildren: () => import('./club/club.module').then(m => m.ClubModule)
    },
    {
        path: 'news',
        loadChildren: () => import('./news/news.module').then(m => m.NewsModule)
    },
    {
        path: 'tickets',
        loadChildren: () => import('./ticket/ticket.module').then(m => m.TicketModule)
    },
    {
        path: 'shop',
        loadChildren: () => import('./shop/shop.module').then(m => m.ShopModule)
    },
    {
        path: 'fondation',
        loadChildren: () => import('./fondation/fondation.module').then(m => m.FondationModule)
    },
    {
        path: 'players',
        loadChildren: () => import('./players/players.module').then(m => m.PlayersModule)
    },
    {
      path: '**', 
      redirectTo: '/home', 
      pathMatch: 'full'
    }
  ];

@NgModule({
    declarations: [
    ],
    imports: [
      RouterModule.forRoot(appRoutes)
    ],
    exports: [RouterModule],
    providers: []
})
export class AppRoutingModule { }