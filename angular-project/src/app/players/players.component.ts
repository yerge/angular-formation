import { Component, OnInit } from '@angular/core';
import { Player } from '../models/player.inteface';
import { PlayerService } from '../services/player.service';

import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';

import { ViewChild } from '@angular/core';

@Component({
  selector: 'app-players',
  templateUrl: './players.component.html',
  styleUrls: ['./players.component.scss']
})
export class PlayersComponent implements OnInit {

  displayedColumns = ["name", "age", "position", "height", "weight", "numberOfGoals", "numberOfYellowCards", "numberOfRedCards", "action"];
  players: Player[] = [];
  dataSource: any;
  selectedRowIndex: string;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private playerService: PlayerService, public dialog: MatDialog) { }

  ngOnInit(): void {
    console.log('ngOnInit');
    this.playerService
      .getPlayers()
      .subscribe((data: Player[]) => {
        console.log('Data', data);
        this.players = data;
        this.dataSource = new MatTableDataSource(this.players);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
      console.log('players', this.players);
  }

  onRowClicked(row) {
    this.selectedRowIndex = row.name;
  }

  remove(player: Player) {
    console.log('delete player', player);
    this.playerService
      .removePlayer(player.id)
      .subscribe((data: Player) => {
        this.players = this.players.filter((lPlayer: Player) => {
          return lPlayer.id !== player.id;
        });
        console.log('delete player after', this.players);
        this.dataSource.data = Object.values(this.players);
        console.log('this.dataSource = ', this.dataSource);
      });
  }
}
