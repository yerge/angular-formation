import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PlayerService } from '../../services/player.service';
import { Player } from '../../models/player.inteface';

@Component({
  selector: 'player-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {
  player: Player;
  submitted = false;

  constructor(private route:ActivatedRoute, private router: Router, private playerService: PlayerService) { }

  ngOnInit(): void {
    this.player = new Player();
  }

  createPlayer() {
    this.playerService.createPlayer(this.player)
      .subscribe(data => console.log(data), error => console.log(error));
      this.player = new Player();
      this.gotoPlayers();
  }

  gotoPlayers() {
    this.router.navigate(['/players']);
  }
  
  onSubmit() {
    this.submitted = true;
    this.createPlayer();    
  }

  newPlayer(): void {
    this.submitted = false;
    this.player = new Player();
  }
}
