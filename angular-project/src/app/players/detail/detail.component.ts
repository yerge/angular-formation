import { Component, OnInit, Input } from '@angular/core';
import {Location} from '@angular/common';
import { Player } from '../../models/player.inteface';
import { ActivatedRoute } from '@angular/router';
import { PlayerService } from '../../services/player.service';

@Component({
  selector: 'player-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {

  player: Player = null;
  constructor(private route:ActivatedRoute,
              private playerService: PlayerService,
              private _location: Location) { }

  ngOnInit(): void {
    const playerId = + this.route.snapshot.paramMap.get('id');
    this.playerService.getPlayer(playerId)
      .subscribe((data: Player) => {
        console.log('player data ', data);
        this.player = data;
      });
  }

  backClicked() {
    this._location.back();
  }
}
