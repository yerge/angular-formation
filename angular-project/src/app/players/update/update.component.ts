import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PlayerService } from '../../services/player.service';
import { Player } from '../../models/player.inteface';

@Component({
  selector: 'player-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.scss']
})
export class UpdateComponent implements OnInit {
  id: number;
  player: Player;

  constructor(private route:ActivatedRoute, private router: Router, private playerService: PlayerService) { }

  ngOnInit(): void {
    this.player = new Player();

    this.id = + this.route.snapshot.paramMap.get('id');

    this.playerService.getPlayer(this.id)
      .subscribe((data: Player) => {
        this.player = data;
      });
  }

  updatePlayer() {
    this.playerService.updatePlayer(this.id, this.player)
    .subscribe(data => console.log(data), error => console.log(error));
    this.player = new Player();
    this.gotoPlayers();
  }

  onSubmit() {
    this.updatePlayer();    
  }

  gotoPlayers() {
    this.router.navigate(['/players']);
  }
}
