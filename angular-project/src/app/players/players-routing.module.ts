import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PlayersComponent } from './players.component';

const routes: Routes = [
    {
      path: '',
     
      children: [
        {
          path: '',
          component: PlayersComponent
        },
        {
          path: 'detail/:id',
          loadChildren: () => import('./detail/detail.module').then(m => m.DetailModule)
        },
        {
          path: 'edit/:id',
          loadChildren: () => import('./update/update.module').then(m => m.UpdateModule)
        },
        {
          path: 'create',
          loadChildren: () => import('./create/create.module').then(m => m.CreateModule)
        },
        {
          path: '**', 
          redirectTo: '', 
          pathMatch: 'full'
        }
      ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PlayersRoutingModule { }