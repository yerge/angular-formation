package com.smartwavesa.angular.formation.springbootmongodbangular.document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Club {
    private String name;
    private int creationYear;
}
