package com.smartwavesa.angular.formation.springbootmongodbangular.repository;

import com.smartwavesa.angular.formation.springbootmongodbangular.document.Player;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PlayerRepository extends MongoRepository<Player, Long> {
}
