package com.smartwavesa.angular.formation.springbootmongodbangular.document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

@Document(collection = "player")
public class Player {

    @Id
    private Long id;
    private String name;
    private int age;
    private String position;
    private double height;
    private double weight;
    private Club club;
    private int numberOfGoals;
    private int numberOfYellowCards;
    private int numberOfRedCards;
}
