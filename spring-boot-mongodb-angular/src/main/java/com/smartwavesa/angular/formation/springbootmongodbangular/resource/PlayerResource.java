package com.smartwavesa.angular.formation.springbootmongodbangular.resource;

import com.smartwavesa.angular.formation.springbootmongodbangular.document.Player;
import com.smartwavesa.angular.formation.springbootmongodbangular.repository.PlayerRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:4201")
@RestController
@RequestMapping("/players")
public class PlayerResource {
    private PlayerRepository playerRepository;

    public PlayerResource(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    @GetMapping("/all")
    public List<Player> getAll() {
        return playerRepository.findAll();
    }

    @GetMapping("/{id}")
    public Player getPlayerById(@PathVariable("id") long id) {
        Optional<Player> player = playerRepository.findById(id);
        if (player.isPresent()) {
            return player.get();
        }
        return null;
    }

    @PostMapping("/")
    public Player createPlayer(@RequestBody Player player) {
        Player playerToSave = playerRepository.save(player);
        return playerToSave;
    }

    @PutMapping("/{id}")
    public Player updatePlayer(@PathVariable("id") long id,
                               @RequestBody Player player) {
        Optional<Player> lPlayer = playerRepository.findById(id);

        if (lPlayer.isPresent()) {
            Player playerToSave = lPlayer.get();
            playerToSave.setAge(player.getAge());
            playerToSave.setClub(player.getClub());
            playerToSave.setHeight(player.getHeight());
            playerToSave.setName(player.getName());
            playerToSave.setPosition(player.getPosition());
            playerToSave.setWeight(player.getWeight());
            playerToSave.setNumberOfGoals(player.getNumberOfGoals());
            playerToSave.setNumberOfYellowCards(player.getNumberOfYellowCards());
            playerToSave.setNumberOfRedCards(player.getNumberOfRedCards());

            playerRepository.save(playerToSave);
            return playerToSave;
        }

        return null;
    }

    @DeleteMapping("/{id}")
    public void deletePlayer(@PathVariable("id") long id) {
        playerRepository.deleteById(id);
    }

}
