package com.smartwavesa.angular.formation.springbootmongodbangular;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootMongodbAngularApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootMongodbAngularApplication.class, args);
	}

}
